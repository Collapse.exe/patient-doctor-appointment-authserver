package kte.testTask.patientdoctorappointment;

import kte.testTask.patientdoctorappointment.model.Doctor;
import kte.testTask.patientdoctorappointment.model.UserAccount;
import kte.testTask.patientdoctorappointment.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;


@SpringBootApplication
public class PatientDoctorAppointmentApplication implements CommandLineRunner {
	@Autowired private UserAccountRepository userAccountRepository;
	@Autowired private PasswordEncoder encoder;

	public static void main(String[] args) {
		SpringApplication.run(PatientDoctorAppointmentApplication.class, args);
	}

	@Override
	public void run(String... args){
		UserAccount admin = new UserAccount(
				"admin", encoder.encode("admin"),
				List.of("ROLE_ADMIN"),
				true
		);
		userAccountRepository.save(admin);
		UserAccount user = new UserAccount(
				"user", encoder.encode("user"),
				List.of("ROLE_USER"),
				true
		);
		userAccountRepository.save(user);
		userAccountRepository.findAll().forEach(System.out::println);
		System.out.println("Приложение запущено.");
	}
}
