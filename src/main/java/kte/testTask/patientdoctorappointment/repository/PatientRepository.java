package kte.testTask.patientdoctorappointment.repository;

import kte.testTask.patientdoctorappointment.model.CompositeId;
import kte.testTask.patientdoctorappointment.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PatientRepository extends JpaRepository<Patient, CompositeId> {
    public Patient findByUuid(UUID uuid);
}
