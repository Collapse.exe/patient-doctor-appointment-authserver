package kte.testTask.patientdoctorappointment;

import com.fasterxml.jackson.databind.ObjectMapper;
import kte.testTask.patientdoctorappointment.model.AppointmentInfo;

import java.io.File;
import java.io.IOException;

public class Converter {

    private static final  String baseFile = "Ids.json";

    public static void toJson(AppointmentInfo info) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.writeValue(new File(baseFile), info);
        System.out.println("Json created!");
    }
}
