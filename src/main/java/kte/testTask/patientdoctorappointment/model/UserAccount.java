package kte.testTask.patientdoctorappointment.model;

import jakarta.persistence.*;
import kte.testTask.patientdoctorappointment.repository.DoctorRepository;
import kte.testTask.patientdoctorappointment.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Entity
public class UserAccount implements UserDetails {

    /*@Autowired
    private PatientRepository patientRepository;
    @Autowired
    DoctorRepository doctorRepository;*/
    @Id private String username;
    private String password;
    private List<String> authorities;
    private boolean enable;
    private String personUUID;
    /*@OneToOne
    private Person uniqueUser;*/

    public UserAccount() {
    }

    public UserAccount(String username, String password, List<String> authorities, boolean enable) {
        this.username = username;
        this.password = password;
        this.authorities = authorities;
        this.enable = enable;
        /*if (doctorRepository.findByUuid(personUUID) == null){
            this.uniqueUser = patientRepository.findByUuid(personUUID);
        }
        else this.uniqueUser = doctorRepository.findByUuid(personUUID);*/
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    public void setAuthorities(List<String> authorities) {
        this.authorities = authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return enable;
    }

    @Override
    public boolean isAccountNonLocked() {
        return enable;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return enable;
    }

    @Override
    public boolean isEnabled() {
        return enable;
    }

    @Override
    public String toString() {
        return "UserAccount{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", authorities=" + authorities +
                ", enable=" + enable +
                ", personUUID=" + personUUID +
                '}';
    }
}
