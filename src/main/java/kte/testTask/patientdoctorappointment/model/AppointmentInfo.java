package kte.testTask.patientdoctorappointment.model;

import java.util.UUID;

public class AppointmentInfo {
    private Long ticketId;
    private Long docId;
    private Long patId;
    private UUID docUUID;
    private UUID patUUID;

    public AppointmentInfo() {
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public Long getDocId() {
        return docId;
    }

    public void setDocId(Long docId) {
        this.docId = docId;
    }

    public Long getPatId() {
        return patId;
    }

    public void setPatId(Long patId) {
        this.patId = patId;
    }

    public UUID getDocUUID() {
        return docUUID;
    }

    public void setDocUUID(UUID docUUID) {
        this.docUUID = docUUID;
    }

    public UUID getPatUUID() {
        return patUUID;
    }

    public void setPatUUID(UUID patUUID) {
        this.patUUID = patUUID;
    }
}
