package kte.testTask.patientdoctorappointment.model;


import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import java.util.UUID;

public abstract class Person {
    private Long id;
    private UUID uuid;
    private String name;
}
