package kte.testTask.patientdoctorappointment.controller;


import kte.testTask.patientdoctorappointment.model.AppointmentInfo;
import kte.testTask.patientdoctorappointment.model.Ticket;
import kte.testTask.patientdoctorappointment.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/lk")
public class TicketController {

    @Autowired
    private TicketService service;

    @GetMapping("/getFreeTickets/{id}.{uuid}.{year}.{month}.{day}")
    public List<Ticket> getAllFreeTickets(@PathVariable Long id,@PathVariable String uuid, @PathVariable int year, @PathVariable int month, @PathVariable int day){
        return service.getFreeSlotsList(id,uuid, year, month, day);
    }
    @GetMapping("/getMyTickets/{id}")
    public List<Ticket> getAllMyTickets(@PathVariable String id){
        return service.getAllMyTickets(id);
    }
    @PostMapping("/appoint")
    public ResponseEntity<Ticket> postAppointment(@RequestBody AppointmentInfo info){
        service.appointmentMake(info);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    @GetMapping("/getAll")
    public List<Ticket> getAllTickets(){
        return service.getAllTickets();
    }
}
